import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:sekolahku/project/EditSiswa.dart';
import 'dart:convert';
import 'dart:async';

import 'package:sekolahku/project/ListSiswa.dart';

class ViewSiswa extends StatefulWidget{
  
  // menampung data
  final List list;
  final int index;
  ViewSiswa({this.index, this.list});
  // end menerima data
  _ViewSiswa createState() => _ViewSiswa();

}

class _ViewSiswa extends State<ViewSiswa>{

  // fungsi untuk menghapus data siswa
  void deleteData(){
    var url = "http://10.0.2.2/flutter-sekolahku-server/siswa/deletesiswa.php";
    http.post(url, body:{
      'id' : widget.list[widget.index]['id']
    });
  }
  // fungsi konfirmasi hapus
  void confirm(){
    AlertDialog alertDialog = new AlertDialog(
      content: Text("Apakah Anda yakin ingin menghapus siswa yang bernama '${widget.list[widget.index]['nama']}'?"),
      actions: <Widget>[
        RaisedButton(
          child: Text("Delete", style: TextStyle(color: Colors.white),),
          color: Colors.red,
          onPressed: (){
            deleteData();
            Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) { 
                return new ListSiswa();      
            }));
          },
        ),
        new RaisedButton(
          child: Text("Close", style:TextStyle(color: Colors.white),),
          color: Colors.green,
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
    showDialog(context: context, child: alertDialog);
  }

  Widget build(BuildContext context){

    // view
    return Scaffold(
      
      appBar: AppBar(
        title: Text("Detail Siswa"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => {
            Navigator.of(context).pushReplacement(
              new MaterialPageRoute(
                builder: (_) {
                  return new ListSiswa();
                }
              ),
            ),
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () => confirm(),
          )
        ],
      ),

      body: Container(
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person),
              title: Text("${widget.list[widget.index]["nama"]}"),
              subtitle: Text("Nama"),
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Icon(Icons.school),
              title: Text("${widget.list[widget.index]["kelas"]} ${widget.list[widget.index]["jenjang"]}"),
              subtitle: Text("Kelas"),
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Icon(Icons.sort),
              title: Text("${widget.list[widget.index]["usia"]} tahun"),
              subtitle: Text("Usia"),
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text("${widget.list[widget.index]["telp"]}"),
              subtitle: Text("Telp"),
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Icon(Icons.child_care),
              title: Text("${widget.list[widget.index]["hobi"].replaceAll(",", ", ")}"),
              subtitle: Text("Hobi"),
            ),
            Divider(
              color: Colors.grey,
            ),
            ListTile(
              leading: Icon(Icons.map),
              title: Text("${widget.list[widget.index]["alamat"]}"),
              subtitle: Text("Alamat"),
            ),
            Divider(
              color: Colors.grey,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        onPressed: () => {
          Navigator.of(context).push(
            new MaterialPageRoute(
              builder: (_) {
                return EditSiswa(list: widget.list, index: widget.index,);
              }
            ),
          ),
        },
      ),
    );

  }

}