import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
// Memanggil depedensi untuk memanipulasi data server
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:sekolahku/project/ListSiswa.dart';
 
class AddSiswa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Tambah Siswa';
 
    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => {
            // kembali ke list siswa
            Navigator.of(context).pushReplacement(
              new MaterialPageRoute(
                builder: (_) {
                  return new ListSiswa();
                }
              ),
            ),
          },
        ),
      ),
      body: FormAddSiswa(),
    );
  }
}
 
class FormAddSiswa extends StatefulWidget {
 
  @override
  _AddSiswa createState() => _AddSiswa();
 
}
 
class _AddSiswa extends State<FormAddSiswa> {
 
  // Buat list untuk radio
  String _jenjang = "TK";
  bool isChecked = true;
  final _formKey = GlobalKey<FormState>();
  
  // Buat list untuk checkbox
  List<String> hobi = ["Membaca", "Menulis", "Menggambar"];

  // Buat controller untuk menampung setiap data yang diinputkan dari text field
  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerTelp = new TextEditingController();
  TextEditingController controllerKelas = new TextEditingController();
  TextEditingController controllerUsia = new TextEditingController();
  TextEditingController controllerAlamat = new TextEditingController();

  void addData() {
    var url = "http://10.0.2.2/flutter-sekolahku-server/siswa/addsiswa.php";
    http.post(url, body: {
      "nama" : controllerNama.text,
      "telp" : controllerTelp.text,
      "kelas" : controllerKelas.text,
      "usia" : controllerUsia.text,
      "alamat" : controllerAlamat.text,
      "jenjang" : _jenjang,
      "hobi" : "${hobi.toString()}",
    });
    // clear data
    clear();
  }

  clear() {
    // controllerTelp = null;
    _formKey.currentState?.reset();
  }
 
  Widget build(BuildContext context){
 
    final Size screenSize = MediaQuery.of(context).size;
    
   
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            TextFormField(
              controller: controllerNama,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Nama Siswa",
                icon: Icon(Icons.person),
                labelText: "Nama Siswa *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Nama wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerKelas,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Kelas",
                icon: Icon(Icons.school),
                labelText: "Kelas *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Kelas wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerUsia,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Usia",
                icon: Icon(Icons.assessment),
                labelText: "Usia *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Usia wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerTelp,
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Phone",
                icon: Icon(Icons.phone),
                labelText: "Phone *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Phone wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerAlamat,
              keyboardType: TextInputType.multiline,
              maxLines: 4,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Alamat",
                icon: Icon(Icons.room),
                labelText: "Alamat *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Alamat wajib diisi";
                }
                return null;
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            Text(
              "Jenjang",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            DropdownButton<String>(
              isExpanded: true,
              value: _jenjang,
              onChanged: (String jenjang){
                setState(() {
                  _jenjang = jenjang;
                });
              },
              items: <String>[
                'TK',
                'SD',
                'SMP',
                'SMA'
              ].map<DropdownMenuItem<String>>(
                (String value){
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }
              ).toList(),
             
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            Text(
              "Hobi",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            CheckboxGroup(
              labels: <String>[
                "Membaca",
                "Menulis",
                "Menggambar",
              ],
              onSelected: (List<String> checked) => setState((){
                hobi = checked;
                print("checked: ${checked.toString()}");
                print(checked);
              }),        
            ),
            Container(
              width: screenSize.width,
              child: new RaisedButton(
                child: new Text(
                  "Simpan",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: (){
                  if (_formKey.currentState.validate()) {
                    print("save");
                    addData();
                  }
                },
                color: Colors.blue,
              ),
              margin: EdgeInsets.only(top: 20),
            ),
          ],
        ),
      ),
    );
 
  }
 
}