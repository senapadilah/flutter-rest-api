import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:http/http.dart' as http;
import 'package:sekolahku/project/ListSiswa.dart';
 
class EditSiswa extends StatefulWidget {
  final List list;
  final int index;
  EditSiswa({this.list, this.index});
  @override
  _EditSiswaState createState() => _EditSiswaState();
}
 
class _EditSiswaState extends State<EditSiswa> {
 
  TextEditingController controllerNama;
  TextEditingController controllerUsia;
  TextEditingController controllerKelas;
  TextEditingController controllerTelp;
  TextEditingController controllerAlamat;
 
  String _jenjang;
  List<String> _hobi;

  List<String> hobi = ["Membaca", "Menulis", "Menggambar"];

  final _formKey = GlobalKey<FormState>();
 
  void editData() {
    var url="http://10.0.2.2/flutter-sekolahku-server/siswa/editsiswa.php";
    http.post(url, body: {
      "id" : widget.list[widget.index]['id'],
      "nama" : controllerNama.text,
      "usia" : controllerUsia.text,
      "kelas" : controllerKelas.text,
      "telp" : controllerTelp.text,
      "alamat" : controllerAlamat.text,
      "jenjang" : _jenjang,
      "hobi" : "${_hobi.toString()}",
    });  
    }
  @override
  void initState() {
    controllerNama = new TextEditingController(text: widget.list[widget.index]['nama']);
    controllerUsia = new TextEditingController(text: widget.list[widget.index]['usia']);
    controllerKelas = new TextEditingController(text: widget.list[widget.index]['kelas']);
    controllerTelp = new TextEditingController(text: widget.list[widget.index]['telp']);
    controllerAlamat = new TextEditingController(text: widget.list[widget.index]['alamat']);
    var controllerHobi = widget.list[widget.index]['hobi'];
    // Menggambar,Menulis
    var valueHobi = controllerHobi.split(",");
    // valueHobi[0] = Menggambar
    // valueHobi[1] = Menulis
    _hobi = valueHobi;
    // _hobi = ["Menulis", "Menggambar"]
    _jenjang = widget.list[widget.index]['jenjang'];
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: new Text("Edit Siswa"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.update,
              color: Colors.white,
            ),
            tooltip: "Save",
            onPressed: () {
              editData();
              Navigator.of(context).pushReplacement(
                new MaterialPageRoute(
                  builder: (_) { 
                    return new ListSiswa();      
                  }
                ),
              );
            },
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
          children: <Widget>[
            new TextFormField(
              controller: controllerNama,
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                hintText: "Nama Siswa", 
                labelText: "Nama *",
                icon: Icon(Icons.person),
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Nama Siswa wajib diisi";
                }
                return null;
              },
            ),
            new TextFormField(
              controller: controllerUsia,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                icon: Icon(Icons.assessment),
                labelText: "Usia *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Usia Siswa wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerKelas,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Kelas Siswa",
                icon: Icon(Icons.school),
                labelText: "Kelas *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Kelas Siswa wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerTelp,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Telp Siswa",
                icon: Icon(Icons.phone),
                labelText: "Telp *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Telp Siswa wajib diisi";
                }
                return null;
              },
            ),
            TextFormField(
              controller: controllerAlamat,
              keyboardType: TextInputType.multiline,
              maxLines: 2,
              decoration: InputDecoration(
                hintText: "Alamat Siswa",
                icon: Icon(Icons.room),
                labelText: "Alamat *",
              ),
              validator: (val){
                if(val.isEmpty){
                  return "Alamat Siswa wajib diisi";
                }
                return null;
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            Text(
              "Jenjang:", 
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            DropdownButton<String>(
              isExpanded: true,
              value: _jenjang,
              onChanged: (String jenjang) {
                setState(() {
                  _jenjang = jenjang;
                });
              },
              items: <String>[
                'TK',
                'SD',
                'SMP'
              ].map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
            Padding(
              padding: EdgeInsets.only(top: 8.0),
            ),
            Text(
              "Hobi:", 
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            CheckboxGroup(
              labels: <String>[
                "Membaca",
                "Menulis",
                "Menggambar",
              ],
              checked: _hobi,
              onChange: (bool isChecked, String label, int index) => print("isChecked: $isChecked   label: $label  index: $index"),
              onSelected: (List<String> checked) => setState((){
                _hobi = checked;
                print("checked: ${checked.toString()}");
                print(_hobi);
              }),
            ),
            Container(
              width: screenSize.width,
              child: new RaisedButton(
                color: Colors.lightBlue,
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    editData();
                    Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) { 
                      return new ListSiswa();      
                    }));
                  }
                },
                
                child: new Text(
                  "Update",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
        ),
      ),
    );
  }
}